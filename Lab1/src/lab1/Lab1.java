/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

/**
 *
 * @author admin
 */
public class Lab1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long numberStudentBook = 0x43D0615; //71108117
        long numberPhone = 971234567; //without 380
        long last2numbers = 0b1000011; //67
        long last4number = 010727; //4567
        int numberInBook = 20;
        int resultBook = FindNumber(numberInBook) + 1;
        System.out.println("Result is " + resultBook);
        char symb = (char)resultBook;
        System.out.println("For student book: ");
        ShowEvenOdd(numberStudentBook);
        System.out.println("For number phone: ");
        ShowEvenOdd(numberPhone);
        System.out.println("For last two numbers: ");
        ShowEvenOdd(last2numbers);
        System.out.println("For last four numbers: ");
        ShowEvenOdd(last4number);
        System.out.println("For result ID in book: ");
        ShowEvenOdd(resultBook);
        System.out.println("Count 1 in student book: " + 
                CountFirst(numberStudentBook));
        System.out.println("Count 1 in phone number: " + 
                CountFirst(numberPhone));
        System.out.println("Count 1 in last 2 numbers: " + 
                CountFirst(last2numbers));
        System.out.println("Count 1 in last 4 numbers: " + 
                CountFirst(last4number));
        System.out.println("Count 1 in result ID book: " + 
                CountFirst(resultBook));
    }
    
    private static int CountFirst(long number)
    {
        int count = 0;
        String translated = Long.toBinaryString(number);
        for(int i = 0; i < translated.length(); i++)
        {
            if(translated.toCharArray()[i] == '1')
                count++;
        }
        
        return count;
    }
    
    private static void ShowEvenOdd(long number)
    {
        System.out.println("Count even: " + FindEven(number) + 
                "\nCount odd: " + FindOdd(number));
    }
    
    private static int FindNumber(int num)
    {
        final int CONST = 26;
        return (num - 1) % CONST;
    }
    
    private static int FindEven(long num)
    {
        int countEven = 0; //четные
        while(num > 0)
        {
            if((num % 10) % 2 == 0)
                countEven++;
            num = num / 10;
        }
        
        return countEven;
    }
    
    private static int FindOdd(long num)
    {
        int countOdd = 0;
        while(num > 0)
        {
            if((num % 10) % 2 == 1)
                countOdd++;
            num /= 10;
        }
        return countOdd;
    }
}
